#!/bin/bash
#停止服务
systemctl stop etcd && \
systemctl disable etcd && \
systemctl daemon-reload

# 删除旧的数据

rm -rvf /data/etcd/* && rm -rvf /var/lib/etcd/*

# 删除 `ssl` 证书
rm -rvf /etc/kubernetes/pki/etcd/*.pem

# 删除配置文件
rm -rvf /etc/etcd/etcd.conf

