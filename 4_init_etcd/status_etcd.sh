#!/bin/bash
# 检查健康状态
systemctl status etcd|grep Active
# 检查成员信息
export ETCDCTL_API=3
etcdctl --cacert=/etc/etcd/ssl/etcd-ca.pem \
--cert=/etc/etcd/ssl/etcd-server.pem \
--key=/etc/etcd/ssl/etcd-server-key.pem \
--endpoints="https://192.168.1.73:2379,https://192.168.1.152:2379,https://192.168.1.245:2379" \
endpoint status health --write-out="table"
