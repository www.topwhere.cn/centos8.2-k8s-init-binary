#!/bin/bash

# 1 创建目录
mkdir -p /var/lib/etcd/ && mkdir -p /data/etcd/ && chmod -R 777 /data/etcd/ && chmod -R 777 /var/lib/etcd/

# 2 服务创建 etcd-02
cat > /etc/etcd/etcd.conf <<EOF
ETCD_NAME=etcd02
ETCD_DATA_DIR="/data/etcd/"

ETCD_LISTEN_CLIENT_URLS="https://192.168.1.152:2379,https://127.0.0.1:2379"

ETCD_LISTEN_PEER_URLS="https://192.168.1.152:2380"

ETCD_INITIAL_ADVERTISE_PEER_URLS="https://192.168.1.152:2380"

ETCD_INITIAL_CLUSTER="etcd01=https://192.168.1.73:2380,etcd02=https://192.168.1.152:2380,etcd03=https://192.168.1.245:2380"

ETCD_INITIAL_CLUSTER_STATE="new"
ETCD_INITIAL_CLUSTER_TOKEN="etcd-cluster"
ETCD_ADVERTISE_CLIENT_URLS="https://192.168.1.152:2379"


ETCD_CLIENT_CERT_AUTH="true"
ETCD_TRUSTED_CA_FILE="/etc/etcd/ssl/etcd-ca.pem"
ETCD_CERT_FILE="/etc/etcd/ssl/etcd-server.pem"
ETCD_KEY_FILE="/etc/etcd/ssl/etcd-server-key.pem"

ETCD_PEER_CLIENT_CERT_AUTH="true"
ETCD_PEER_TRUSTED_CA_FILE="/etc/etcd/ssl/etcd-ca.pem"
ETCD_PEER_CERT_FILE="/etc/etcd/ssl/etcd-server.pem"
ETCD_PEER_KEY_FILE="/etc/etcd/ssl/etcd-server-key.pem"
EOF

# 3 创建启动服务
cat > /usr/lib/systemd/system/etcd.service <<EOF
[Unit]
Description=Etcd Server
After=network.target

[Service]
Type=simple
WorkingDirectory=/var/lib/etcd/
EnvironmentFile=-/etc/etcd/etcd.conf
# set GOMAXPROCS to number of processors
ExecStart=/bin/bash -c "GOMAXPROCS=$(nproc) /usr/local/bin/etcd"
Type=notify

[Install]
WantedBy=multi-user.target
EOF


# 4 启动服务
systemctl daemon-reload && \
systemctl start etcd && \
systemctl enable etcd
