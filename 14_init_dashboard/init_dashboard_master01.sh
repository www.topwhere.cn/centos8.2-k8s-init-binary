#!/bin/bash

kubectl  apply -f dashboard.yaml
kubectl  apply -f dashboard-user.yaml
#sed -i "s#ClusterIP#clusterIP: 10.96.0.10#g"  /home/centos8.2-k8s-init-binary/k8s-ha-install/CoreDNS/coredns.yaml


#更改dashboard的svc为NodePort，如果已是请忽略
kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard
  type: NodePort

kubectl -n kubernetes-dashboard create token admin-user
