#!/bin/bash

# 1 安装相关包
yum install gcc systemd-devel keepalived haproxy -y

# 2 配置 haproxy
cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg_bak
cat >/etc/haproxy/haproxy.cfg<<EOF
#---------------------------------------------------------------------
# Example configuration for a possible web application.  See the
# full configuration options online.
#
#   https://www.haproxy.org/download/1.8/doc/configuration.txt
#
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    # to have these messages end up in /var/log/haproxy.log you will
    # need to:
    #
    # 1) configure syslog to accept network log events.  This is done
    #    by adding the '-r' option to the SYSLOGD_OPTIONS in
    #    /etc/sysconfig/syslog
    #
    # 2) configure local2 events to go to the /var/log/haproxy.log
    #   file. A line like the following can be added to
    #   /etc/sysconfig/syslog
    #
    #    local2.*                       /var/log/haproxy.log
    #
    log         127.0.0.1 local5 info

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

    # utilize system-wide crypto-policies
    ssl-default-bind-ciphers PROFILE=SYSTEM
    ssl-default-server-ciphers PROFILE=SYSTEM

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    tcp
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         5000
    timeout client          50000
    timeout server          50000
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000


listen status_page
    bind 0.0.0.0:8443
    bind 127.0.0.1:8443
    balance roundrobin
    stats enable

    stats realm "Welcome to the haproxy load balancer status page of YinZhengjie"
    stats hide-version
    stats admin if TRUE
    stats refresh 5s
    #URI相对地址
    stats uri  /k8s
    #账号密码
    stats auth    admin:admin
    #统计报告格式
    stats realm   Global\ statistics

    #服务地址
    server k8s-master01 192.168.1.73:6443 check
    server k8s-master02 192.168.1.152:6443 check
    server k8s-node01 192.168.1.152:6443 check
EOF

# 3 启动 haproxy
systemctl daemon-reload && systemctl restart haproxy && systemctl enable haproxy

# 4 增加健康检查
cat >/etc/keepalived/check_apiserver.sh<<EOF
#!/bin/bash
haproxy_status=\`ps -C haproxy --no-header | wc -l\`
if [ \$haproxy_status -eq 0 ]; then
    echo "systemctl stop keepalived"
    /usr/bin/systemctl stop keepalived
    exit 1
fi
EOF
chmod 755 /etc/keepalived/check_apiserver.sh

# 5 配置master节点
cat >/etc/keepalived/keepalived.conf<<EOF
global_defs {
    router_id LVS_DEVEL
}

vrrp_script chk_apiserver {
    script "/etc/keepalived/check_apiserver.sh"
    interval 5
    weight -5
    fall 2
    rise 1
}

vrrp_instance VI-kube-master {
    state MASTER #需要将状态设置为(MASTER)
    interface eth0
    virtual_router_id 10
    priority 101
    nopreempt
    advert_int 2
    authentication {
        auth_type PASS
        auth_pass K8SHA_KA_AUTH
    }
    virtual_ipaddress {
        192.168.1.11
    }
    track_script {
        chk_apiserver
    }
}
EOF

# 6 启动 keepalived
systemctl daemon-reload && systemctl start keepalived && systemctl enable keepalived
systemctl status keepalived
curl https://10.98.94.183:443