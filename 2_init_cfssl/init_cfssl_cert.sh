#!/bin/bash
#生成配置文件
# 修改ca-config.json,分别配置针对三种不同证书类型的profile,其中有效期43800h为5年
cat > /etc/etcd/ssl/ca-config.json << EOF
{
    "signing": {
        "default": {
            "expiry": "87600h"
        },
        "profiles": {
            "kubernetes": {
                "expiry": "87600h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth",
                    "client auth"
                ]
            },
            "server": {
                "expiry": "87600h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth"
                ]
            }
        }
    }
}
EOF
# 2 生成 ca-csr.json 文件(自签CA)
cat > /etc/etcd/ssl/ca-csr.json  << EOF
  {
      "CN": "kubernetes",
      "hosts": [
        "127.0.0.1",
        "192.168.1.11",
        "192.168.1.73",
        "192.168.1.152",
        "192.168.1.245"
        ],
      "key": {
          "algo": "rsa",
          "size": 2048
      },
      "names": [
          {
              "C": "CN",
              "L": "Beijing",
              "ST": "Beijing",
              "O": "k8s",
              "OU": "System"
          }

      ]
  }
EOF
# 生成etcd相关证书
cfssl gencert -initca /etc/etcd/ssl/ca-csr.json | cfssljson -bare /etc/etcd/ssl/etcd-ca -
cat > /etc/etcd/ssl/etcd-server-csr.json<<EOF
{
  "CN": "etcd-server",
  "hosts": [
  "127.0.0.1",
  "192.168.1.11",
  "192.168.1.73",
  "192.168.1.152",
  "192.168.1.245"
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "Beijing",
      "L": "Beijing",
      "O": "etcd-cluster",
      "OU": "system"
    }
  ]
}
EOF
# 生成etcd-server证书
cfssl gencert \
-ca=/etc/etcd/ssl/etcd-ca.pem \
-ca-key=/etc/etcd/ssl/etcd-ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-hostname=127.0.0.1,k8s-master01,k8s-master02,k8s-node01,192.168.1.11,192.168.1.73,192.168.1.152,192.168.1.245 \
-profile=kubernetes \
/etc/etcd/ssl/etcd-server-csr.json | cfssljson -bare /etc/etcd/ssl/etcd-server


#生成k8s组件跟证书
cfssl gencert -initca /etc/etcd/ssl/ca-csr.json | cfssljson -bare /etc/kubernetes/pki/ca -
#生成api-server证书
# 2 生成 apiserver-server 证书配置文件
cat > /etc/etcd/ssl/apiserver-server-csr.json<<EOF
{
    "CN": "kubernetes",
    "hosts": [
          "10.96.0.1",
          "127.0.0.1",
    		  "192.168.1.11",
          "192.168.1.73",
          "192.168.1.152",
          "192.168.1.245",
          "kubernetes",
          "kubernetes.default",
          "kubernetes.default.svc",
          "kubernetes.default.svc.cluster",
          "kubernetes.default.svc.cluster.local"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "BeiJing",
            "L": "BerJing",
            "O": "k8s",
            "OU": "System"
        }
        ]
}
EOF
cfssl gencert \
-ca=/etc/kubernetes/pki/ca.pem \
-ca-key=/etc/kubernetes/pki/ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-hostname=10.96.0.1,127.0.0.1,kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.default.svc.cluster.local,192.168.1.11,192.168.1.73,192.168.1.152,192.168.1.245 \
-profile=kubernetes \
/etc/etcd/ssl/apiserver-server-csr.json | cfssljson -bare /etc/kubernetes/pki/apiserver-server
#
## 2-3.创建 etcd 提供给 kube-apiserver 访问的 client 证书
cat > /etc/etcd/ssl/apiserver-client-csr.json<<EOF
{
    "CN": "kubernetes",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Beijing",
            "L": "Beijing",
            "O": "k8s",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert \
-ca=/etc/kubernetes/pki/ca.pem \
-ca-key=/etc/kubernetes/pki/ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-profile=kubernetes /etc/etcd/ssl/apiserver-client-csr.json | \
cfssljson -bare /etc/kubernetes/pki/apiserver-client




# 生成apiserver的聚合证书proxy
cfssl gencert -initca /etc/etcd/ssl/ca-csr.json | cfssljson -bare /etc/kubernetes/pki/proxy-ca -
cat > /etc/etcd/ssl/proxy-csr.json<<EOF
{
    "CN": "kubernetes",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Beijing",
            "L": "Beijing",
            "O": "k8s",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert \
-ca=/etc/kubernetes/pki/proxy-ca.pem \
-ca-key=/etc/kubernetes/pki/proxy-ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-profile=kubernetes /etc/etcd/ssl/proxy-csr.json | \
cfssljson -bare /etc/kubernetes/pki/proxy
#生成controller—manager证书
cat > /etc/etcd/ssl/controller-manager-csr.json<<EOF
{
    "CN": "system:kube-controller-manager",
     "hosts": [
      "127.0.0.1",
      "192.168.1.73",
      "192.168.1.11",
      "192.168.1.152",
      "192.168.1.245"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "BeiJing",
            "L": "BerJing",
            "O": "system:kube-controller-manager",
            "OU": "Kubernetes-manual"
        }
        ]
}
EOF

cfssl gencert \
-ca=/etc/kubernetes/pki/ca.pem \
-ca-key=/etc/kubernetes/pki/ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-profile=kubernetes \
/etc/etcd/ssl/controller-manager-csr.json | cfssljson -bare /etc/kubernetes/pki/controller-manager


rm -rf /etc/kubernetes/pki/kube-controller-manager.kubeconfig
#配置controller-manager 的 kubectl config 文件
# (1) 设置一个集群项
kubectl config set-cluster kubernetes \
--certificate-authority=/etc/kubernetes/pki/ca.pem \
--embed-certs=true \
--server=https://192.168.1.11:8443 \
--kubeconfig=/etc/kubernetes/pki/kube-controller-manager.kubeconfig

# (2) 设置一个用户项
kubectl config set-credentials system:kube-controller-manager@kubernetes \
--client-key=/etc/kubernetes/pki/controller-manager-key.pem \
--client-certificate=/etc/kubernetes/pki/controller-manager.pem \
--embed-certs=true \
--kubeconfig=/etc/kubernetes/pki/kube-controller-manager.kubeconfig

# (3) 设置一个环境项，一个上下文
kubectl config set-context system:kube-controller-manager@kubernetes \
--cluster=kubernetes \
--user=system:kube-controller-manager \
--kubeconfig=/etc/kubernetes/pki/kube-controller-manager.kubeconfig

# (4) 使用某个环境当作默认环境
kubectl config use-context system:kube-controller-manager@kubernetes --kubeconfig=/etc/kubernetes/pki/kube-controller-manager.kubeconfig
#生成scheduler证书

cat > /etc/etcd/ssl/scheduler-csr.json<<EOF
{
    "CN": "system:kube-scheduler",
    "hosts": [
    "127.0.0.1",
    "192.168.1.11",
    "192.168.1.73",
    "192.168.1.152",
    "192.168.1.245"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "BeiJing",
            "L": "BerJing",
            "O": "system:kube-scheduler",
            "OU": "Kubernetes-manual"
        }
        ]
}
EOF

# 2 生成 scheduler 证书
cfssl gencert \
-ca=/etc/kubernetes/pki/ca.pem \
-ca-key=/etc/kubernetes/pki/ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-profile=kubernetes \
/etc/etcd/ssl/scheduler-csr.json | cfssljson -bare /etc/kubernetes/pki/scheduler

rm -rf /etc/kubernetes/pki/scheduler.kubeconfig
# 3 配置 scheduler 的 kubectl config 文件
# (1) 设置一个集群项
kubectl config set-cluster kubernetes \
--certificate-authority=/etc/kubernetes/pki/ca.pem \
--embed-certs=true \
--server=https://192.168.1.11:8443 \
--kubeconfig=/etc/kubernetes/pki/scheduler.kubeconfig

# (2) 设置一个用户项
kubectl config set-credentials system:kube-scheduler@kubernetes \
--client-key=/etc/kubernetes/pki/scheduler-key.pem \
--client-certificate=/etc/kubernetes/pki/scheduler.pem \
--embed-certs=true \
--kubeconfig=/etc/kubernetes/pki/scheduler.kubeconfig

# (3) 设置一个环境项，一个上下文
kubectl config set-context system:kube-scheduler@kubernetes \
--cluster=kubernetes \
--user=system:kube-scheduler \
--kubeconfig=/etc/kubernetes/pki/scheduler.kubeconfig

# (4) 使用某个环境当作默认环境
kubectl config use-context system:kube-scheduler@kubernetes --kubeconfig=/etc/kubernetes/pki/scheduler.kubeconfig


# 生成admin证书及配置
cat > /etc/etcd/ssl/admin-csr.json<<EOF
{
    "CN": "admin",
    "hosts": [
        "127.0.0.1",
        "192.168.1.11",
        "192.168.1.73",
        "192.168.1.152",
        "192.168.1.245"
        ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Beijing",
            "L": "Beijing",
            "O": "system:masters",
            "OU": "Kubernetes-manual"
        }
    ]
}
EOF

# 2 生成 admin-csr 证书
cfssl gencert \
-ca=/etc/kubernetes/pki/ca.pem \
-ca-key=/etc/kubernetes/pki/ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-profile=kubernetes /etc/etcd/ssl/admin-csr.json | \
cfssljson -bare /etc/kubernetes/pki/admin

rm -rf /etc/kubernetes/pki/admin.kubeconfig /root/.kube/config

# 3 配置 admin-csr 的 kubectl config 文件
# (1) 设置一个集群项
kubectl config set-cluster kubernetes \
--certificate-authority=/etc/kubernetes/pki/ca.pem \
--embed-certs=true \
--server=https://192.168.1.11:8443 \
--kubeconfig=/etc/kubernetes/pki/admin.kubeconfig

# (2) 设置一个用户项
kubectl config set-credentials kubernetes-admin \
--client-key=/etc/kubernetes/pki/admin-key.pem \
--client-certificate=/etc/kubernetes/pki/admin.pem \
--embed-certs=true \
--kubeconfig=/etc/kubernetes/pki/admin.kubeconfig

# (3) 设置一个环境项，一个上下文
  kubectl config set-context kubernetes-admin@kubernetes \
  --cluster=kubernetes \
  --user=kubernetes-admin \
  --kubeconfig=/etc/kubernetes/pki/admin.kubeconfig

# (4) 使用某个环境当作默认环境
kubectl config use-context kubernetes-admin@kubernetes --kubeconfig=/etc/kubernetes/pki/admin.kubeconfig



cat > /etc/etcd/ssl/kube-proxy-csr.json<<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "Beijing",
      "L": "Beijing",
      "O": "system:kube-proxy",
      "OU": "Kubernetes-manual"
    }
  ]
}
EOF


cfssl gencert \
-ca=/etc/kubernetes/pki/ca.pem \
-ca-key=/etc/kubernetes/pki/ca-key.pem \
-config=/etc/etcd/ssl/ca-config.json \
-profile=kubernetes \
/etc/etcd/ssl/kube-proxy-csr.json | cfssljson -bare /etc/kubernetes/pki/kube-proxy

# 4 生成openssl key
openssl genrsa -out /etc/kubernetes/pki/sa.key 2048
openssl rsa -in /etc/kubernetes/pki/sa.key -pubout -out /etc/kubernetes/pki/sa.pub

mkdir -p /root/.kube && cp /etc/kubernetes/pki/admin.kubeconfig /root/.kube/config
#kubectl create -f bootstrap.secret.yaml
#

# 授权
#kubectl create clusterrolebinding cluster-system-anonymons --clusterrole=cluster-admin --user=system:anonymous

MasterNodes='k8s-master01 k8s-master02'

WorkNodes='k8s-node01'

for NODE in $MasterNodes;do
ssh $NODE "mkdir -p /etc/etcd/ssl/"
	scp /etc/etcd/ssl/* $NODE:/etc/etcd/ssl/
done
for NODE in $MasterNodes;do
ssh $NODE "mkdir -p /etc/kubernetes/pki/"
	scp /etc/kubernetes/pki/* $NODE:/etc/kubernetes/pki/
done

for NODE in $MasterNodes;do
ssh $NODE "mkdir -p /root/.kube"
	scp /root/.kube/config $NODE:/root/.kube/
done


for NODE in $WorkNodes;do
ssh $NODE "mkdir -p /root/.kube"
	scp /root/.kube/config $NODE:/root/.kube/
done



for NODE in $WorkNodes;do
ssh $NODE "mkdir -p /etc/kubernetes/"
	scp /etc/kubernetes/* $NODE:/etc/kubernetes/
done
for NODE in $WorkNodes;do
ssh $NODE "mkdir -p /etc/etcd/ssl/"
	scp /etc/etcd/ssl/* $NODE:/etc/etcd/ssl/
done
for NODE in $WorkNodes;do
ssh $NODE "mkdir -p /etc/kubernetes/pki/"
  scp /etc/kubernetes/pki/* $NODE:/etc/kubernetes/pki/
done

