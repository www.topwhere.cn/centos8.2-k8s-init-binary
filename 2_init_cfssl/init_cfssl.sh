#!/bin/bash

cd /home
# 安装kubernetes
wget https://dl.k8s.io/v1.25.4/kubernetes-server-linux-amd64.tar.gz
tar -xf kubernetes-server-linux-amd64.tar.gz --strip-component=3 -C /usr/local/bin/ kubernetes/server/bin/kube{let,ctl,-apiserver,-controller-manager,-scheduler,-proxy}
kubelet --version

# 安装etcd
wget https://github.com/etcd-io/etcd/releases/download/v3.4.13/etcd-v3.4.13-linux-amd64.tar.gz
tar -xf etcd-v3.4.13-linux-amd64.tar.gz --strip-components=1 -C /usr/local/bin/ etcd-v3.4.13-linux-amd64/etcd{,ctl}
etcdctl version

# 安装cfssl生成证书组件
wget http://hw.files.jiankangyouyi.com/cfssl_linux-amd64 -O /usr/local/bin/cfssl
wget http://hw.files.jiankangyouyi.com/cfssl-certinfo_linux-amd64 -O /usr/local/bin/cfssl-certinfo
wget http://hw.files.jiankangyouyi.com/cfssljson_linux-amd64 -O /usr/local/bin/cfssljson
chmod +x /usr/local/bin/cfssl /usr/local/bin/cfssljson /usr/local/bin/cfssl-certinfo
