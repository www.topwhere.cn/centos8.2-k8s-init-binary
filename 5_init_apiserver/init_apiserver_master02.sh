#!/bin/bash


# 1 配置master-2
cat >/usr/lib/systemd/system/kube-apiserver.service<<EOF
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]

ExecStart=/usr/local/bin/kube-apiserver \\
  --v=2 \\
  --logtostderr=true \\
  --allow-privileged=true \\
  --bind-address=0.0.0.0 \\
  --secure-port=6443 \\
  --advertise-address=192.168.1.152 \\
  --service-cluster-ip-range=10.96.0.0/12 \\
  --service-node-port-range=30000-32767 \\
  --etcd-servers=https://192.168.1.73:2379,https://192.168.1.152:2379,https://192.168.1.245:2379 \\
  --etcd-cafile=/etc/etcd/ssl/etcd-ca.pem \\
  --etcd-certfile=/etc/etcd/ssl/etcd-server.pem \\
  --etcd-keyfile=/etc/etcd/ssl/etcd-server-key.pem \\
  --client-ca-file=/etc/kubernetes/pki/ca.pem \\
  --tls-cert-file=/etc/kubernetes/pki/apiserver-server.pem  \\
  --tls-private-key-file=/etc/kubernetes/pki/apiserver-server-key.pem \\
  --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-client.pem \\
  --kubelet-client-key=/etc/kubernetes/pki/apiserver-client-key.pem \\
  --service-account-key-file=/etc/kubernetes/pki/sa.pub \\
  --service-account-signing-key-file=/etc/kubernetes/pki/sa.key \\
  --service-account-issuer=https://kubernetes.default.svc.cluster.local \\
  --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname  \\
  --enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,NodeRestriction,ResourceQuota  \\
  --authorization-mode=Node,RBAC  \\
  --enable-bootstrap-token-auth=true \\
  --requestheader-client-ca-file=/etc/kubernetes/pki/proxy-ca.pem  \\
  --proxy-client-cert-file=/etc/kubernetes/pki/proxy.pem  \\
  --proxy-client-key-file=/etc/kubernetes/pki/proxy-key.pem  \\
  --requestheader-allowed-names=aggregator  \\
  --requestheader-group-headers=X-Remote-Group  \\
  --requestheader-extra-headers-prefix=X-Remote-Extra-  \\
  --requestheader-username-headers=X-Remote-User

#--token-auth-file=/etc/kubernetes/pki/token.csv
#--enable-aggregator-routing=true \\

Restart=on-failure
RestartSec=10s
LimitNOFILE=65535

[Install]
WantedBy=multi-user.target
EOF


# 2 运行
systemctl daemon-reload && systemctl restart kube-apiserver && systemctl enable --now kube-apiserver

systemctl status kube-apiserver
