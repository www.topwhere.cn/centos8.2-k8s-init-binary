#!/bin/bash
#停止服务
systemctl stop kube-apiserver && \
systemctl disable kube-apiserver && \
systemctl daemon-reload

# 删除 `ssl` 证书
rm -rvf /etc/etcd/ssl/apiserver-ca*.pem
rm -rvf /etc/kubernetes/pki/apiserver-server*.pem
rm -rvf /etc/kubernetes/pki/apiserver-client*.pem
rm -rvf /etc/kubernetes/pki/apiserver-proxy-client*.pem


