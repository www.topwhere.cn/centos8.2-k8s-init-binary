#!/bin/bash
systemctl daemon-reload && systemctl stop etcd

systemctl daemon-reload && systemctl stop kube-apiserver
systemctl daemon-reload && systemctl stop kube-controller-manager
systemctl daemon-reload && systemctl stop kube-scheduler
tail -f /var/log/messages



