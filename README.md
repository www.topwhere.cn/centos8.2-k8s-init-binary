# centos8.2-k8s-init

#### 介绍
centos8.2环境k8s初始化-sh安装教程

#### 软件架构
centos8.2- k8s1.25.2
1 master
2 nodo




#### 安装教程

##### 一.拉取git代码（k8s最新版本增加 cri-dockerd 这个包不加跑不起来，）
```
yum install -y git
cd /home

git clone https://gitee.com/www.topwhere.cn/centos8.2-k8s-init-binary.git
cd centos8.2-k8s-init-binary
chmod -R 777 ./*
```

##### 二.执行 ./init.sh

##### 三.【master节点安装】
            执行 ./init.sh
            将init-master.sh 中ip换成自身master机器ip,执行 ./init-master.sh


##### 四.【nodo节点安装】
            执行 ./init.sh
            将init-nodo1.sh 中ip换成自身nodo机器ip,密钥换成master返回的密钥，执行 ./init-nodo.sh

##### 五.【添加秘钥并发送到各个节点】
 ```
ssh-keygen -t rsa
cd ~
for i in k8s-master01 k8s-master02 k8s-node01;do ssh-copy-id -i .ssh/id_rsa.pub $i;done
```


