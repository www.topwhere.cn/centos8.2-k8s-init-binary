#!/bin/bash
# 5 配置master
cat >/usr/lib/systemd/system/kube-controller-manager.service<<EOF
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes
After=network.target

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
      --v=2 \\
      --logtostderr=true \\
      --bind-address=127.0.0.1 \\
      --root-ca-file=/etc/kubernetes/pki/ca.pem \\
      --service-account-private-key-file=/etc/kubernetes/pki/ca-key.pem \\
      --cluster-name=kubernetes  \\
      --cluster-signing-cert-file=/etc/kubernetes/pki/ca.pem \\
      --cluster-signing-key-file=/etc/kubernetes/pki/ca-key.pem \\
      --kubeconfig=/etc/kubernetes/pki/kube-controller-manager.kubeconfig \\
      --leader-elect=true \\
      --tls-cert-file=/etc/kubernetes/pki/controller-manager.pem \\
      --tls-private-key-file=/etc/kubernetes/pki/controller-manager-key.pem \\
      --use-service-account-credentials=true \\
      --node-monitor-grace-period=40s \\
      --node-monitor-period=5s \\
      --pod-eviction-timeout=2m0s \\
      --controllers=*,bootstrapsigner,tokencleaner \\
      --allocate-node-cidrs=true \\
      --cluster-cidr=172.16.0.0/12 \
      --requestheader-client-ca-file=/etc/kubernetes/pki/proxy-ca.pem \\
      --node-cidr-mask-size=24

Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF

# 授权
kubectl create clusterrolebinding cluster-system-anonymons --clusterrole=cluster-admin --user=system:anonymous

# 6 启动
systemctl daemon-reload && systemctl restart kube-controller-manager && systemctl enable --now kube-controller-manager
#查看状态
systemctl status kube-controller-manager
#systemctl stop kube-controller-manager
#systemctl daemon-reload && systemctl restart kube-controller-manager
