#!/bin/bash

hostnamectl set-hostname k8s-node01


# 2.添加host
cat >> /etc/hosts << EOF
127.0.0.1 k8s-node01
192.168.1.73 k8s-master01
192.168.1.152 k8s-master02
192.168.1.245 k8s-node01
EOF

# 3 永久生效
cat >>  /etc/sysconfig/network << EOF
HOSTNAME=k8s-node01
EOF
# 4 更新环境
bash

