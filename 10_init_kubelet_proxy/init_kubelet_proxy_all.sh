#!/bin/bash

#创建一个 kube-proxy 的 service account:
kubectl -n kube-system create serviceaccount kube-proxy
#将 kube-proxy 的 serviceaccount 绑定到 clusterrole system:node-proxier 以允许 RBAC：
kubectl create clusterrolebinding system:kube-proxy \
 --clusterrole system:node-proxier \
 --serviceaccount kube-system:kube-proxy

#
#SECRET=$(kubectl -n kube-system get sa/kube-proxy \
#--output=jsonpath='{.secrets[0].name}')
#
#JWT_TOKEN=$(kubectl -n kube-system get secret/$SECRET \
#--output=jsonpath='{.data.token}' | base64 -d)
#

#创建kube-proxy的kubeconfig:
kubectl config set-cluster kubernetes \
--certificate-authority=/etc/kubernetes/pki/ca.pem \
--embed-certs=true \
--server=https://192.168.1.11:8443 \
--kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig

kubectl config set-credentials kubernetes \
--token=c8ad9c.2e4d610cf3e7426e \
--kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig

#kubectl config set-credentials kubernetes \
#--client-certificate=/etc/kubernetes/pki/kube-proxy.pem     \
#--client-key=/etc/kubernetes/pki/kube-proxy-key.pem     \
#--embed-certs=true     \
#--kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig


kubectl config set-context kubernetes \
--cluster=kubernetes \
--user=kubernetes \
--kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig

kubectl config use-context kubernetes --kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig


### --- 创建ClusterRoleBinding
#kubectl create clusterrolebinding system:kube-proxy  --clusterrole system:node-proxier --serviceaccount kube-system:kube-proxy





#cat > /etc/kubernetes/kube-proxy.conf << EOF
#KUBE_PROXY_OPTS="--logtostderr=false \\
#--v=2 \\
#--log-dir=/home/k8s/logs \\
#--config=/etc/kubernetes/kube-proxy-config.yml"
#EOF

cat > /etc/kubernetes/kube-proxy.yaml << EOF
apiVersion: kubeproxy.config.k8s.io/v1alpha1
bindAddress: 0.0.0.0
clientConnection:
  acceptContentTypes: ""
  burst: 10
  contentType: application/vnd.kubernetes.protobuf
  kubeconfig: /etc/kubernetes/kube-proxy.kubeconfig
  qps: 5
clusterCIDR: 172.16.0.0/12
configSyncPeriod: 15m0s
conntrack:
  max: null
  maxPerCore: 32768
  min: 131072
  tcpCloseWaitTimeout: 1h0m0s
  tcpEstablishedTimeout: 24h0m0s
enableProfiling: false
healthzBindAddress: 0.0.0.0:10256
hostnameOverride: ""
iptables:
  masqueradeAll: false
  masqueradeBit: 14
  minSyncPeriod: 0s
  syncPeriod: 30s
ipvs:
  masqueradeAll: true
  minSyncPeriod: 5s
  scheduler: "rr"
  syncPeriod: 30s
kind: KubeProxyConfiguration
metricsBindAddress: 127.0.0.1:10249
mode: "ipvs"
nodePortAddresses: null
oomScoreAdj: -999
portRange: ""
udpIdleTimeout: 250ms
EOF





cat >/usr/lib/systemd/system/kube-proxy.service <<EOF

[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/kubernetes/kubernetes
After=network.target
[Service]
ExecStart=/usr/local/bin/kube-proxy \
  --config=/etc/kubernetes/kube-proxy.yaml \
  --v=2

Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF


systemctl daemon-reload && systemctl restart kube-proxy && systemctl enable --now kube-proxy
systemctl status kube-proxy
