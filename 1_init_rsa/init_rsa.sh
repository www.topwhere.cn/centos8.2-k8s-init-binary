#!/bin/bash

# 添加秘钥并发送到各个节点
ssh-keygen -t rsa
cd ~
for i in k8s-master01 k8s-master02 k8s-node01;do ssh-copy-id -i .ssh/id_rsa.pub $i;done