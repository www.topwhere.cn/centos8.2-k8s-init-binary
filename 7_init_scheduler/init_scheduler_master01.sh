#!/bin/bash
# 5 配置master
cat >/usr/lib/systemd/system/kube-scheduler.service<<EOF
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes
After=network.target

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
      --v=2 \\
      --logtostderr=true \\
      --bind-address=127.0.0.1 \\
      --leader-elect=true \\
      --kubeconfig=/etc/kubernetes/pki/scheduler.kubeconfig

Restart=always
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF


# 6 启动
systemctl daemon-reload && systemctl start kube-scheduler && systemctl enable --now kube-scheduler
systemctl restart kube-scheduler
#查看状态
systemctl status kube-scheduler